/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package ru.ael.lesson5types;

/**
 *
 * @author developer
 */
public class Lesson5Types {

    public static void main(String[] args) {
        System.out.println("Целочисленные типы данных");

        long b = 1555500000L; // 8 байт 
        System.out.println("Значение переменной b  = " + b); 
        
        int count = 5; // 4 байта обявление переменной и указание значения
        System.out.println("Значение переменной count  = " + count);  // преобразование cout  в строку  ctrl + пробел
        
        short a;  // 2 байта объявление переменной a 

        a = 2000; // запись в переменную a значения 2000 диапазон от -32768  до 32767
        System.out.println("Значение переменной a  = " + a);  

        a = 15;
        System.out.println("Значение переменной a  = " + a);

        
        byte  с  = 127; // 1 байт диапазон от -127  до 127
        System.out.println("Значение переменной с = " + с);
        
        double pow = Math.pow(с, 2.5);
        System.out.println("Значение переменной с^2.5 = " + pow);
        
        System.out.println("Числа с плавающей точкой");
          
        float f = 3.14F;  // 4 байта 
        System.out.println("Значение переменной f  = " + f);
        
        double d = 124.786;  // 8 байтов
        System.out.println("Значение переменной f  = " + d);
        System.out.println("Резульаты математических операций: ");
        System.out.println("Положительная бесконечность:  "+Double.POSITIVE_INFINITY);
        System.out.println("Отричательная бесконечность:  "+Double.NEGATIVE_INFINITY);
        System.out.println("Не является числом:  "+Double.NaN); // NaN not a number
        
        
        System.out.println("Проверка Резульатов математических операций: ");
        if (Double.isNaN(d)) {
            System.out.println(" d Не является числом:  " + Double.NaN); // NaN not a number
        } else {

            System.out.println(" d является числом:  " + d); // NaN not a number
        }
        
        System.out.println("Деление числа на ноль:");
        double p = 12.6;
        double m = p / 0;
        if (Double.isInfinite(m)) {
            System.out.println(" m является положительной бесконечностью  " + Double.POSITIVE_INFINITY);
            
        } else {

            System.out.println(" m является числом:  " + m); 
        }
        System.out.println("Расчёт косинуса 360 градусов");
        double x = 360.0;
        double z = Math.cos(x);
        System.out.println("Косинус " + x +  " градусов =" + z);
        
        System.out.println("Расчет синуса 180 градусов");
        double l = 180;
        double q = Math.sin(l);
        System.out.println("Синус " + l + " градусов = " + q);
        
       
        double s = 1.5;
        System.out.println("Проверка больше ли число " + s + " чем ноль" );
        if (s > 0) {
            System.out.println("Число " + s + " больше ноля");
        } else {
             System.out.println("Число " + s + " меньше ноля");
        }
        
        double v = s*(-1);
        System.out.println("Проверка больше ли число " + v + " чем ноль" );
        if (v > 0) {
            System.out.println("Число " + v + " больше ноля");
        } else {
             System.out.println("Число " + v + " меньше ноля");
        }
        
        
        String o= "Хабаровск";
        System.out.println(o); //приведение типов на самостоятельное изучение. строки, ввод вывод 6 урок
        
        
    }
}
